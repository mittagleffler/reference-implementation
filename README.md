# Generalized Mittag-Leffler function

This is a the reference implementation of the algorithm presented in
```
SEYBOLD, H., & HILFER, R. (2008). NUMERICAL ALGORITHM FOR CALCULATING THE GENERALIZED MITTAG-LEFFLER FUNCTION. SIAM Journal on Numerical Analysis, 47(1), 69–88. http://www.jstor.org/stable/25663114
```
for computing the generalized Mittag-Leffler function. It is written in Pure Python, and should be used for debugging purposes only.

## Install

You can install the package using pip:
```
python -m pip install git+https://gitlab.com/mittagleffler/reference-implementation.git
```

## Usage

Basic usage:
```
>>> from mittleff import mittleff
>>> mittleff(1, 1, 0.6) # => 1.822118800390509 
```

## Interactive debugging

```
>>> %load_ext autoreload
>>> %autoreload 2
>>> import logging
>>> logging.basicConfig(level=logging.DEBUG)
>>> from mittleff import mittleff
>>> logging.getLogger('mittleff').setLevel(logging.DEBUG)
>>> mittleff(1, 1, 0.6) # => 1.822118800390509 
```

## Testing

Use [Poetry](https://python-poetry.org/) to install the project dependencies:
```
$ poetry install
$ poetry run pytest -rP
```

